<?php

namespace GetRepo\BehatPantherExtension\ServiceContainer\Driver;

use Behat\Mink\Driver\CoreDriver;
use Behat\Mink\Driver\PantherDriver as BasePantherDriver;

class PantherDriver extends BasePantherDriver
{
    private \Behat\Mink\Driver\CoreDriver $javascriptDriver;

    public function setKernelDriver(CoreDriver $driver): void
    {
        $this->javascriptDriver = $driver;
    }

    public function getKernelDriver(): CoreDriver
    {
        return $this->javascriptDriver;
    }
}
