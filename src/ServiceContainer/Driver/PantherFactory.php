<?php

declare(strict_types=1);

namespace GetRepo\BehatPantherExtension\ServiceContainer\Driver;

use Behat\MinkExtension\ServiceContainer\Driver\DriverFactory;
use GetRepo\BehatPantherExtension\ServiceContainer\PantherConfiguration;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\HttpKernelBrowser;

class PantherFactory implements DriverFactory
{
    /**
     * {@inheritdoc}
     */
    public function getDriverName(): string
    {
        return 'panther';
    }

    /**
     * {@inheritdoc}
     */
    public function supportsJavascript(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function configure(ArrayNodeDefinition $builder): void
    {
        $configuration = new PantherConfiguration();
        $builder
            ->children()
                ->scalarNode('bootstrap')->end()
            ->end();
        $builder->append($configuration->addOptionsNode());
        $builder->append($configuration->addKernelOptionsNode());
        $builder->append($configuration->addManagerOptionsNode());
    }

    /**
     * {@inheritdoc}
     */
    public function buildDriver(array $config): Definition
    {
        $_ENV['APP_ENV'] = 'test';
        $_ENV['APP_DEBUG'] = 0;
        $_ENV['PANTHER_APP_ENV'] = 'test';

        if ($config['bootstrap'] ?? false) {
            $bootstrap = $config['bootstrap'];
            require_once getcwd() . '/' . $bootstrap;
        }

        // symfony kernel
        $kernelDefinition = new Definition(
            'App\Kernel',
            ['test', false]
        );

        // HTTP kernel browser client
        $httpKernelBrowserDefinition = new Definition(
            HttpKernelBrowser::class,
            [$kernelDefinition]
        );

        // kernel driver
        $kernelDriverDefinition = new Definition(
            KernelDriver::class,
            [$httpKernelBrowserDefinition]
        );
        $kernelDriverDefinition->addMethodCall(
            'setKernel',
            [$kernelDefinition]
        );

        // JS Panther browser client
        $pantherDriverDefinition = new Definition(
            PantherDriver::class,
            [
                $config['options'] ?? [],
                $config['kernel_options'] ?? [],
                $config['manager_options'] ?? [],
            ]
        );
        $pantherDriverDefinition->addMethodCall(
            'setKernelDriver',
            [$kernelDriverDefinition]
        );

        return $pantherDriverDefinition;
    }
}
