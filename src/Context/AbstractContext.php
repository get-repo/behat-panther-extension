<?php

namespace GetRepo\BehatPantherExtension\Context;

use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Mink\Exception\ElementTextException;
use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use GetRepo\BehatPantherExtension\ServiceContainer\Driver\PantherDriver;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class AbstractContext extends MinkContext
{
    private static ?Mink $kernelDriver = null;
    private static KernelInterface $kernel;

    public static function setKernel(KernelInterface $kernel): void
    {
        self::$kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenarioHook(BeforeScenarioScope $event): void
    {
        if (!\in_array('javascript', $event->getScenario()->getTags())) {
            if (!self::$kernelDriver) {
                $mink = clone $this->getMink();
                $sessionJs = new Session(
                    $mink->getSession()->getDriver()->getKernelDriver(), // @phpstan-ignore-line
                    $mink->getSession()->getSelectorsHandler()
                );
                self::$kernelDriver = new Mink();
                $name = $mink->getDefaultSessionName();
                self::$kernelDriver->registerSession($name, $sessionJs);
                self::$kernelDriver->setDefaultSessionName($name);
                if (!self::$kernelDriver->getSession()->getDriver()->isStarted()) {
                    self::$kernelDriver->getSession()->getDriver()->start();
                }
            }
            $this->setMink(self::$kernelDriver);
        }
    }

    /**
     * @AfterScenario
     */
    public function afterScenarioHook(AfterScenarioScope $event): void
    {
        $driver = $this->getSession()->getDriver();
        $isPanther = $driver instanceof PantherDriver;

        if (!$event->getTestResult()->isPassed()) {
            $logdir = self::$kernel->getLogDir();
            $file = $event->getFeature()->getFile();
            $line = $event->getScenario()->getLine();
            if ($isPanther) {
                $path = \sprintf(
                    '%s/behat_screenshot_%s_%s:%s.png',
                    dirname($logdir),
                    \date('YmdHis'),
                    \basename($file),
                    $line
                );
                $driver->getScreenshot($path);
            }
        }
    }

    /**
     * @AfterSuite
     */
    public static function afterSuiteHook(AfterSuiteScope $event): void
    {
        if (self::$kernelDriver && self::$kernelDriver->getSession()->getDriver()->isStarted()) {
            self::$kernelDriver->getSession()->getDriver()->stop();
        }
    }

    /**
     * Example: When I wait for the "#test-id" element.
     *
     * @Given /^I wait for the "(?P<selector>[^"]*)" element$/
     */
    public function iWaitForElement(string $selector): void
    {
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof PantherDriver) {
            $driver->getClient()->waitFor($selector);
        }
    }

    /**
     * Example: When I wait for 2 seconds.
     *
     * @When /^I wait for (?P<seconds>(\d|\.)+) seconds?$/
     */
    public function iWaitForXSeconds(int $seconds): void
    {
        sleep($seconds);
    }

    /**
     * Empty field with specified id|name|label|value
     * Example: When I empty field "username"
     *
     * @When /^(?:|I )empty field "(?P<field>(?:[^"]|\\")*)"$/
     */
    public function emptyField(string $field): void
    {
        $page = $this->getSession()->getPage();
        $field = $page->findField($this->fixStepArgument($field));

        while (!empty($field->getValue())) {
            // backspaces
            $field->keyPress(8);
            $field->keyPress(8);
        }
    }

    /**
     * Checks, that element content is equal to specific value
     * Example: Then the "#test" element text equals "string".
     *
     * @Then /^the "(?P<selector>[^"]*)" element text (?P<comparison>(equals|contains)) "(?P<value>(?:[^"]|\\")*)"$/
     */
    public function assertElementEquals(string $selector, string $comparison, string $value): void
    {
        $assert = $this->assertSession();
        $element = $assert->elementExists('css', $selector);
        $actual = trim($element->getText());
        switch ($comparison) {
            case 'equals':
                if ($actual !== $value) {
                    $message = "'{$value}' is not equal to '{$actual}'";
                    throw new ElementTextException($message, $this->getSession()->getDriver(), $element);
                }
                break;
            case 'contains':
                if (!$value) {
                    $message = 'contains value is empty';
                    throw new ElementTextException($message, $this->getSession()->getDriver(), $element);
                }
                if (!str_contains($actual, $value)) {
                    $message = "'{$value}' is not in '{$actual}'";
                    throw new ElementTextException($message, $this->getSession()->getDriver(), $element);
                }
                break;
        }
    }

    /**
     * Checks, that at least (?P<num>\d+) CSS elements exist on the page
     * Example: Then I should see at least 5 "div" elements
     * Example: And I should see at least 5 "div" elements.
     *
     * @Then /^(?:|I )should see (at least) (?P<num>\d+) "(?P<element>[^"]*)" elements?$/
     */
    public function assertComparisonNumElements(int $num, ?string $comparison, string $element): void
    {
        $session = $this->getSession();
        $nb = count($session->getPage()->findAll('css', $element));
        $num = \intval($num);

        if ($nb < $num) {
            $message = \sprintf(
                '%d element(s) matching css "%s", but should be %s %d',
                $nb,
                $element,
                $comparison,
                $num
            );
            throw new ExpectationException($message, $session->getDriver());
        }
    }

    /**
     * Example: When I click on the "#id-element" element.
     *
     * @When /^(?:|I )click on the "(?P<selector>[^"]*)" element$/
     */
    public function iClickOnElement(string $selector): void
    {
        $element = $this->assertSession()->elementExists('css', $selector);
        $element->click();
    }

    /**
     * @When /^(?:|I )(?P<action>(accept|dismiss)) the alert$/
     */
    public function acceptJavascriptAlert(string $action): void
    {
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof PantherDriver) {
            $driver->getClient()->getWebDriver()->switchTo()->alert()->$action();
        }
    }

    /**
     * @When /^(?:|I )should see "(?P<expected>[^"]*)" in the alert$/
     */
    public function assertJavascriptAlertText(string $expected): void
    {
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof PantherDriver) {
            $actual = $driver->getClient()->getWebDriver()->switchTo()->alert()->getText();

            if (!str_contains($actual, $expected)) {
                $message = \sprintf(
                    'Javascript alert does not contains "%s", got "%s"',
                    $expected,
                    $actual,
                );
                throw new ExpectationException($message, $driver);
            }
        }
    }

    /**
     * Example: Then debug
     * Example: Then debug html
     * Example: Then debug text
     * Example: Then debug json
     *
     * @When /^debug(?P<type> (html|text|json))?$/
     */
    public function debug(string $type = 'html'): void
    {
        $type = trim($type);
        if ('json' === $type) {
            $json = $this->getSession()->getPage()->getContent();
            echo @json_encode(@json_decode($json, true, 512, JSON_THROW_ON_ERROR), JSON_PRETTY_PRINT);
        } else {
            $method = sprintf('get%s', ucfirst($type));
            echo $this->getSession()->getPage()->$method();
        }
    }
}
