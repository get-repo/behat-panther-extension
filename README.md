<p align="center">
    <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/17968928/image844-2.png" height=100 />
</p>

<h1 align=center>Behat Panther Extension</h1>
<h4 align=center style="border-bottom: 3px solid #008033; padding-bottom: 15px;">
    BehatPantherExtension is an integration layer between Behat and Panther.
</h4>

<br/>
What is Behat ? [behat](https://docs.behat.org/en/latest/)<br/>
What is Panther ? [symfony/panther](https://github.com/symfony/panther)<br/>

## Table of Contents

1. [Installation](#installation)
1. [Configuration Reference](#configuration-reference)
1. [Abstract Context](#abstract-context)

<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/behat-panther-extension git https://gitlab.com/get-repo/behat-panther-extension.git
    composer require get-repo/behat-panther-extension

and the BehatPantherExtension in your behat.yaml:
```yaml
default:
    extensions:
        GetRepo\BehatPantherExtension: ~
```
See the [configuration reference](#configuration-reference) for more details.

<br/><br/>
## Configuration Reference

behat.yaml:
```yaml
default:
    extensions:
        GetRepo\BehatPantherExtension:
            kernel:
                bootstrap: config/bootstrap.php
                class: App\Kernel
                env: test
                debug: false
        Behat\MinkExtension:
            sessions:
                default:
                    panther:
                        #  to use an external web server
                        # (the PHP built-in web server will not be started)
                        external_base_uri: 'http://custom.local'
                        # to change the project's document root
                        # (default to ./public/, relative paths must start by ./)
                        webServerDir: '/specific/path'
                        hostname: 'custom.local'
                        # to change the web server's port (default to 9080)
                        port: 9085
                        # to use a web server router script
                        router: '/specific/path/to/router.php'
                        browser: 'chrome' # chrome or firefox
                        environmentVariables:
                            # to override the APP_ENV variable
                            PANTHER_APP_ENV: test
```

<br/><br/>
## Abstract Context

This repository provides an abstract context to enable features to help you to test with Panther:<br/>
This is the class `GetRepo\BehatPantherExtension\Context\AbstractContext`

Exemple:
```php
<?php

namespace Tests;

use GetRepo\BehatPantherExtension\Context\AbstractContext;

class FeatureContext extends AbstractContext
{

}

```

### @javascript tag

You can add the `@javascript` tag to yout test to enable panther on-demand.

```gherkin
Feature: My test

Scenario: My Scenario without panther
    Given I am on "/books"
    etc ...

@javascript
Scenario: My Scenario with panther
    Given I am on "/elephants"
    etc ...
```

### Screenshot and logs on failures

If your scenario fails and use the `@javascript` tag, you can find the screenshot in the log dir
of your Symfony project *var/log/behat_screenshot_xxxx.png*.<br/>
Otherwise you can find the log in the log dir too *var/log/behat.log*.


### Waiting steps

If your scenario use the `@javascript` tag, sometimes you need to wait for an element to be displayed,
or just wait feew seconds if you really want too.

```gherkin
When I wait for the "#my-id" element
When I wait for 2 seconds
```

### Asserting steps

Custom asserts:

```gherkin
# assert text contains exactly a string
Then the "#test" element text equals "string"
# assert number of elements
Then I should see 10 "div" elements
Then I should see at least 5 "div" elements
```

### Clicking step

```gherkin
When I click on the "#my-id" element
```

### Debug step

```gherkin
# display the full context
Then debug
# display the full html page
Then debug html
# display the full text (from html)
Then debug text
```


